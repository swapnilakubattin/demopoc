# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact




package com.santalu.maskedittext

import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputEditText

/**
 * Created
 */

class MaskTextWatcher(
  private val editText: TextInputEditText,
  private val mask: String,
  private var listener:MessageListener
) : TextWatcher {
//  private lateinit var listner : MessageListener
  private var selfChange = false

  override fun  afterTextChanged(s: Editable?){
    if (selfChange) return
    selfChange = true
    format(s)
    selfChange = false
  }

  override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
  }

  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
  }

  fun format(text: Editable?) {
    if (text.isNullOrEmpty()) return
    text.apply {
      // reset input filters
      val editableFilters = filters
      filters = emptyArray()

      val formatted = StringBuilder()
      val list = toMutableList()

      // apply mask
      mask.forEach { m ->
        if (list.isNullOrEmpty()) return@forEach
        var c = list[0]
        if (m.isPlaceHolder()) {
          if (!c.isLetterOrDigit()) {
            // find next letter or digit
            val iterator = list.iterator()
            while (iterator.hasNext()) {
              c = iterator.next()
              if (c.isLetterOrDigit()) break
              iterator.remove()
            }
          }
          if (list.isNullOrEmpty()) return@forEach
          formatted.append(c)
          list.removeAt(0)
        } else {
          formatted.append(m)
          if (m == c) {
            list.removeAt(0)
          }
        }
      }
      val previousLength = length
      val currentLength = formatted.length
      replace(0, previousLength, formatted, 0, currentLength)

      if (currentLength == 10){
        if(formatted.toString().matches(Regex("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))\$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$"))){
          listener.ToastMsg("Date is valid")
        }else{
          listener.ToastMsg("Date is invalid")
        }
      }
      // set correct cursor position when editing
      if (currentLength < previousLength) {
        val currentSelection = findCursorPosition(text, editText.selectionStart)
        editText.setSelection(currentSelection)
      }

      // restore input filters
      filters = editableFilters
    }
  }

  fun unformat(text: Editable?): String? {
    if (text.isNullOrEmpty()) return null
    val unformatted = StringBuilder()
    val textLength = text.length
    mask.forEachIndexed { index, m ->
      if (index >= textLength) return@forEachIndexed
      if (m.isPlaceHolder()) {
        unformatted.append(text[index])
      }
    }
    return unformatted.toString()
  }

  private fun findCursorPosition(text: Editable?, start: Int): Int {
    if (text.isNullOrEmpty()) return start
    val textLength = text.length
    val maskLength = mask.length
    var position = start
    for (i in start until maskLength) {
      if (mask[i].isPlaceHolder()) {
        break
      }
      position++
    }
    position++
    return if (position < textLength) position else textLength
  }

  interface MessageListener{
    fun ToastMsg(msg :String)
  }

  fun setListener(messageListener :MessageListener){
    this.listener = messageListener;
  }
}

****************************************************************************


package com.santalu.maskedittext

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText

/**
 * Created by fatihsantalu on 20.11.2018
 */

class MaskEditText : TextInputEditText {


  private lateinit var mask: String

  var maskTextWatcher: MaskTextWatcher? = null

  /*var mask: String? = null
    set(value) {
      field = value
      if (value.isNullOrEmpty()) {
        removeTextChangedListener(maskTextWatcher)
      } else {
        maskTextWatcher = MaskTextWatcher(this, mask!!,listner)
        addTextChangedListener(maskTextWatcher)
      }
    }*/

  val rawText: String?
    get() {
      val formatted = text
      return maskTextWatcher?.unformat(formatted) ?: formatted.toString()
    }

  constructor(context: Context) : super(context)

  constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    init(context, attrs)
  }

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
      super(context, attrs, defStyleAttr) {

    init(context, attrs)
  }

  private fun init(context: Context, attrs: AttributeSet?) {
    attrs?.let {
      val a = context.obtainStyledAttributes(it, R.styleable.MaskEditText)
      with(a) {
        mask = getString(R.styleable.MaskEditText_met_mask)
        recycle()
      }
    }
  }


}
**********************************************************************
internal fun Char.isPlaceHolder(): Boolean = this == '#'




